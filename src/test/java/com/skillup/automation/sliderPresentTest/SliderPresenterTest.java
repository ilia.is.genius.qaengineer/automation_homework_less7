package com.skillup.automation.sliderPresentTest;

import com.skillup.automation.TestRunner;
import org.testng.annotations.Test;

import static com.skillup.automation.utils.RandomEmailString.generateString;

public class SliderPresenterTest extends TestRunner {

    @Test
    public void homePageExistElementsTest() {
        String email = generateString(10);

        sliderPresenterPage
                .open()
                .isPopUpOnPage()
                .clickSignUpButton();


        signUpPage
                .enterEmail(email + "@mailinator.com")
                .clickOnCheckbox()
                .clickOnSignUpButton();

        mailinatorPage
                .open()
                .enterEmail(email)
                .clickEnter()
                .clickMessage()
                .switchToframeLink()
                .clickOnLink()
                .exitFromFrame();

        signInPage
                .switchNewTab()
                .enterFirstName("Ilya")
                .enterLastName("Shylkov");
    }
}
