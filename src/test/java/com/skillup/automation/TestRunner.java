package com.skillup.automation;

import com.skillup.automation.pages.CommonPage;
import com.skillup.automation.pages.MailinatorPage;
import com.skillup.automation.pages.SliderPresenterPage;
import com.skillup.automation.pages.signIn.SignInPage;
import com.skillup.automation.pages.signUp.SignUpPage;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.skillup.automation.configuration.Wait.FIVE_SECONDS;
import static com.skillup.automation.utils.WebDriverFactory.initDriver;
import static com.skillup.automation.utils.WebDriverFactory.setUpBrowsersDriver;

public class TestRunner {

    protected WebDriver driver;
    protected SignUpPage signUpPage;
    protected CommonPage commonPage;
    protected SliderPresenterPage sliderPresenterPage;
    protected MailinatorPage mailinatorPage;
    protected SignInPage signInPage;


    @BeforeSuite
    public void beforeSuite() {
        setUpBrowsersDriver();
    }

    @BeforeClass
    public void beforeClass() {
    }

    @BeforeMethod
    public void beforeMethod() {
        driver = initDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(FIVE_SECONDS, TimeUnit.SECONDS);

        signUpPage = new SignUpPage(driver);
        commonPage = new CommonPage(driver);
        sliderPresenterPage = new SliderPresenterPage(driver);
        mailinatorPage = new MailinatorPage(driver);
        signInPage = new SignInPage(driver);
    }


    @AfterMethod
    public void saveScreenShot(ITestResult result) throws IOException {
        if (!result.isSuccess()) {

            String screenShotName = String.format("screenshot_%s_%s.png", result.getMethod().getMethodName(), System.currentTimeMillis());
            File source = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            String destPath = Paths.get(System.getProperty("user.dir"), "screenshots", screenShotName).toAbsolutePath().toString();

            FileUtils.copyFile(source, new File(destPath));
        }

        if (driver != null) {
            driver.quit();
        }
    }


//    @AfterMethod
//    public void afterMethod() {
//        closeNotUsedTabs();
//        driver.quit();
//    }

    @AfterClass
    public void afterClass() {
    }

    @AfterSuite
    public void afterSuite() {
    }

    private void closeNotUsedTabs() {
        String currentTab = driver.getWindowHandle();
        List<String> allTabs = new ArrayList<String>(driver.getWindowHandles());

        for (int i = 0; i < allTabs.size(); i++) {
            String tab = allTabs.get(i);
            if (!tab.equals(currentTab)) {
                driver.switchTo().window(tab);
                driver.close();
            }
        }

        driver.switchTo().window(currentTab);
    }
}
