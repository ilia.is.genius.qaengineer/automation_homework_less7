package com.skillup.automation.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;

import static com.skillup.automation.configuration.Wait.TEN_SECONDS;

public class CommonPage {
    protected WebDriver driver;

    public CommonPage(WebDriver driver) {
        this.driver = driver;
    }

    public String getUrl() {
        return driver.getCurrentUrl();
    }

    public void enterText(String locator, String text) {
        WebElement input = driver.findElement(find(locator));
        input.clear();
        input.sendKeys(text);
    }

    public void clickEnterButton(String locator) {
        WebElement enter = driver.findElement(find(locator));
        enter.sendKeys(Keys.ENTER);
    }


    public void click(String locator) {
        driver.findElement(find(locator)).click();
    }

    public void clickInTheNewTab(String locator) {
        WebElement element = driver.findElement(find(locator));
        element.sendKeys(Keys.CONTROL, Keys.RETURN);
    }

    public void switchToLastTab () {
        List<String> list = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(list.get(list.size() - 1));
    }

    protected boolean isElementOnPage(String locator, int time) {
        WebDriverWait wait = new WebDriverWait(driver, time);

        try {
            wait.until(ExpectedConditions.visibilityOfElementLocated(find(locator)));
        } catch (TimeoutException e) {
            return false;
        }

        return true;
    }

    protected void switchToframe(String locator) {
        WebElement element = driver.findElement(find(locator));
        driver.switchTo().frame((element));
    }

    protected boolean switchTotop() {
        driver.switchTo().defaultContent();
        return true;
    }

    protected boolean isElementOnPage(String locator) {
        return isElementOnPage(locator, TEN_SECONDS);
    }

    protected boolean waitTillElementClickable(String locator) {
        return waitTillElementClickable(locator, TEN_SECONDS);
    }

    protected boolean waitTillElementClickable(String locator, int seconds) {
        WebDriverWait wait = new WebDriverWait(driver, seconds);

        try {
            wait.until(ExpectedConditions.elementToBeClickable(find(locator)));
        } catch (TimeoutException e) {
            return false;
        }

        return true;
    }


    protected By find(String locator) {
        if (locator.startsWith("//") || locator.startsWith("./")) {
            return By.xpath(locator);
        }

        return By.cssSelector(locator);
    }
}
