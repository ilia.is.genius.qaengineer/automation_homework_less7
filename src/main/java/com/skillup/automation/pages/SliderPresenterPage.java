package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;

import static com.skillup.automation.configuration.Urls.SLIDE_PRESENTER_URL;
import static com.skillup.automation.configuration.Wait.ONE_SECOND;

public class SliderPresenterPage extends CommonPage {
    private static final String SIGN_UP_BUTTON_TO_ENTER_XPATH_LOCATOR = "//a[contains (@class, 'btn-signup')]/span";
    private static final String CHAT_POPUP_XPATH_LOCATOR = "//div[@id = 'collect-chat-frame-wrap']";
    private static final String CHAT_CLOSE_XPATH_LOCATOR = "//div[@title = 'Close']";

    public SliderPresenterPage(WebDriver driver) {
        super(driver);
    }

    public SliderPresenterPage open() {
        driver.get(SLIDE_PRESENTER_URL);
        return this;
    }

    public SliderPresenterPage isPopUpOnPage() {
        if (isElementOnPage(CHAT_POPUP_XPATH_LOCATOR, ONE_SECOND)) {
            click(CHAT_CLOSE_XPATH_LOCATOR);
        }

        return this;
    }

    public SliderPresenterPage clickSignUpButton() {
        click(SIGN_UP_BUTTON_TO_ENTER_XPATH_LOCATOR);
        return this;
    }
}
