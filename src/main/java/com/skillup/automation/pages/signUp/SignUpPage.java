package com.skillup.automation.pages.signUp;

import com.skillup.automation.pages.CommonPage;
import org.openqa.selenium.WebDriver;

public class SignUpPage extends CommonPage {
    private static final String EMAIL_INPUT_FIELD_XPATH_LOCATOR = "//input[contains (@name, 'email')]";
    private static final String CHECKBOX_AGREE_BUTTON_XPATH_LOCATOR = "//i[contains (@class, '_1hO8MtbPtzRSPy3GVztCbW')]";
    private static final String SIGN_UP_ON_PAGE_BUTTON_XPATH_LOCATOR = "//span[(text() = 'Sign up for free')]/..";


    public SignUpPage(WebDriver driver) {
        super(driver);
    }

    public SignUpPage enterEmail(String email) {
        enterText(EMAIL_INPUT_FIELD_XPATH_LOCATOR, email);
        return this;
    }

    public SignUpPage clickOnCheckbox() {
        click(CHECKBOX_AGREE_BUTTON_XPATH_LOCATOR);
        return this;
    }

    public SignUpPage clickOnSignUpButton() {
        click(SIGN_UP_ON_PAGE_BUTTON_XPATH_LOCATOR);
        return this;
    }
}
