package com.skillup.automation.pages.signIn;

import com.skillup.automation.pages.CommonPage;
import org.openqa.selenium.WebDriver;

public class SignInPage extends CommonPage {
    private static final String INPUT_NAME_FIELD_XPATH_LOCATOR = "//input[@name = 'firstName']";
    private static final String INPUT_LAST_NAME_FIELD_XPATH_LOCATOR = "//input[@name = 'lastName']";

    public SignInPage(WebDriver driver) {
        super(driver);
    }

    public SignInPage switchNewTab() {
        switchToLastTab();
        return this;
    }

    public SignInPage enterFirstName(String text) {
        enterText(INPUT_NAME_FIELD_XPATH_LOCATOR, text);
        return this;
    }

    public SignInPage enterLastName(String text) {
        enterText(INPUT_LAST_NAME_FIELD_XPATH_LOCATOR, text);
        return this;
    }

}
