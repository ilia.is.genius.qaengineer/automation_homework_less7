package com.skillup.automation.pages;

import org.openqa.selenium.WebDriver;

import static com.skillup.automation.configuration.Urls.MAILINATOR_URL;

public class MailinatorPage extends CommonPage {
    private static final String EMAIL_INPUT_FIELD_XPATH_LOCATOR = "//input[@id = 'inboxfield']";
    private static final String MESSAGE_DIV_XPATH_LOCATOR = "//tr[1][contains(@class, 'even')]";
    private static final String ACTIVATION_LINK_XPATH_LOCATOR = "//a[contains(text(), 'Activate')]";

    public static final String IFRAME_XPATH_LOCATOR = "//iframe[@name = 'msg_body']";

    public MailinatorPage(WebDriver driver) {
        super(driver);
    }

    public MailinatorPage open() {
        driver.get(MAILINATOR_URL);
        return this;
    }

    public MailinatorPage enterEmail(String email) {
        enterText(EMAIL_INPUT_FIELD_XPATH_LOCATOR, email);
        return this;
    }

    public MailinatorPage clickEnter() {
        clickEnterButton(EMAIL_INPUT_FIELD_XPATH_LOCATOR);
        return this;
    }

    public MailinatorPage clickMessage() {
        waitTillElementClickable(MESSAGE_DIV_XPATH_LOCATOR);
        click(MESSAGE_DIV_XPATH_LOCATOR);
        return this;
    }

    public MailinatorPage switchToframeLink() {
        switchToframe(IFRAME_XPATH_LOCATOR);
        return this;
    }

    public MailinatorPage exitFromFrame() {
        switchTotop();
        return this;
    }

    public MailinatorPage clickOnLink() {
        clickInTheNewTab(ACTIVATION_LINK_XPATH_LOCATOR);
        return this;
    }
}
