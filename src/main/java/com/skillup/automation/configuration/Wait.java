package com.skillup.automation.configuration;

public class Wait {
    public static final int ONE_SECOND = 1;
    public static final int FIVE_SECONDS = 5;
    public static final int TEN_SECONDS = 10;
}
