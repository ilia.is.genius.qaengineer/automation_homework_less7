package com.skillup.automation.configuration;

public class Urls {
    public static final String SLIDE_PRESENTER_URL = "https://slidepresenter.com/en/";
    public static final String MAILINATOR_URL = "https://www.mailinator.com/";
}
